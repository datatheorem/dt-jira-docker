# https://hub.docker.com/_/alpine/
FROM alpine:3.6

# Update and install packages in same command to assure we have the latest ones and not re-use an old layer
RUN apk update && apk add --no-cache \
    # We need openssh-client in order to install private python packages via git+ssh.
    openssh-client git \
    python3 python3-dev \
    # Required for building some of the python packages
    gcc musl-dev libffi-dev openssl-dev

# Make sure pip installed
RUN python3 -m ensurepip

# Upgrade basic python tools.
RUN pip3 install --upgrade pip==9.0.1 virtualenv

# Construct virtualenv.
RUN virtualenv /env -p python3
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH

# Install requirements and only things we want in the container
RUN pip install git+https://bitbucket.com/datatheorem/dt-api-client.git git+https://bitbucket.org/datatheorem/dt-jira-integration.git

COPY . /app/
RUN chmod +x /app/default.sh
RUN crontab /app/default.sh

# Run the cronjob on the foreground
ENTRYPOINT crond -f
