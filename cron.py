#!/usr/bin/env python3
import os
import json
from pathlib import Path
import traceback

import requests

from dt_jira_integration.config import get_jira_config
from dt_jira_integration.config import get_results_api_key
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration
from dt_jira_integration.logger import jira_logger
from dt_jira_integration.sync_with_jira_action import CustomerJiraSyncAction

# Check if we were given a config yaml to use
current_dir = Path(__file__).absolute().parent
config_path = current_dir / 'config.yaml'

try:
    jira_logger.info('Starting Cron')
    if config_path.exists():
        jira_config = JiraIntegrationConfiguration.from_yaml(config_path)
    else:
        jira_config = get_jira_config()

    action = CustomerJiraSyncAction(get_results_api_key(), jira_config)
    action.perform()
    jira_logger.info('Ended Cron')
except:
    slack_webhook_url = os.getenv('SLACK_WEBHOOK_URL')
    if slack_webhook_url:
        headers = {'Content-Type': 'application/json'}
        data = json.dumps({'text': f'Jira cron has reported the following:\n{traceback.format_exc()}'})
        requests.post(slack_webhook_url, data=data, headers=headers)
    raise
