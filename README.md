# DT Jira Integration

## Intro
This project provides a sample/baseline Docker image that can be used as part of an “on-prem” integration between Data
Theorem and a Jira Server instance. Instead of pushing new (or selected manually-exported) issues, it polls Data
Theorem’s Results API to find issues that should be synced with your Jira instance.

Note that Data Theorem does not recommend this approach because we cannot provide the same degree of support as we can
for setting up a Jira integration through our portal (which can use IP address whitelisting). Our standard integration
pushes new issues as they are discovered, and allows you to manually export issues that are not automatically synced
using our portal. This Docker image instead periodically polls our APIs, and the manual export feature is incompatible
with this approach to syncing issues.

If you would like to do everything locally (Including not giving Data Theorem credentials to a jira account) then you
will have to provide a `config.yaml` file with the correct configuration for starting the jira integration process.
This file should be in the same directory as the `Dockerfile`.

The format and configuration details can be found here:

https://bitbucket.org/datatheorem/dt-jira-integration/

Here is a quick example for config yaml file:

```yaml
base_url: 'https://jira.com'
password: 'password'
username: 'username'
export_filter: 'ALL_ISSUES'
export_prod: True
export_pre_prod: False
type_of_issue_name: 'Security'
project_key_or_id: 'SEC'
```


### Create the image
```
docker build -t jiradt .
```

### Run the image and the cron job
```
docker run -e RESULTS_API_KEY="ENTER KEY HERE" -e SLACK_WEBHOOK_URL="SLACK_URL" -d --name jiradt jiradt
```
You can get the results api key from https://www.securetheorem.com/sdlc

You can also pass in a slack webhook url if you'd like to be notified whenever the cron job failed.
The slack webhook url is optional

### Stop the image
```
docker stop jiradt
```

### Customize
To be able to choose when the cron job runs, override the file `default.sh`
with the desired crontab format.

Currently, the cron job is set to run on Tuesday and Thursday at 8:00 AM
